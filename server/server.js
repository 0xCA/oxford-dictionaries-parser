let express = require('express')
let http = require('http')
let bodyParser = require('body-parser')
let fs = require('fs')

// let defFileStream = fs.createWriteStream('DefList.json', { 'flags': 'a' })
let defFileStream = fs.createWriteStream('DefList.csv', { 'flags': 'a' })

let app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ type: 'application/json' }))
app.disable('x-powered-by')

app.post('/saveDef', function (req, res) {
  // let json = JSON.stringify(req.body)
  // defFileStream.write(`${json}\n`)
  let exgString = `"${req.body.exgList.join(', ')}"`
  let synString = `"${req.body.synList.join(', ')}"`
  let csvStr = `${req.body.word} ${req.body.phoneticspelling},,${req.body.definition},${exgString},${synString}`

  defFileStream.write(`${csvStr}\n`)

  res.send(JSON.stringify('ok'))
})

http.createServer(app).listen(80)

console.log('Server is started')

// defFileStream.end('this is the end line')
