// ==UserScript==
// @name         Oxford Dictionaries parser
// @version      0.2
// @author       0xCA
// @match        https://en.oxforddictionaries.com/definition/*
// @grant        GM_xmlhttpRequest
// @connect      127.0.0.1
// @downloadURL  https://gitlab.com/0xCA/oxford-dictionaries-parser/raw/master/oxford.user.js
// @updateURL    https://gitlab.com/0xCA/oxford-dictionaries-parser/raw/master/oxford.user.js
// ==/UserScript==

/* globals GM_xmlhttpRequest */

class Definition {
  constructor (word, phoneticspelling) {
    this.word = word
    this.definition = ''
    this.exgList = []
    this.synList = []
    this.phoneticspelling = phoneticspelling
  }

  addInfo (info) {
    this.definition = info.definition
    this.exgList = info.exgList
    this.synList = info.synList
  }

  toObject () {
    return {
      word: this.word,
      definition: this.definition,
      exgList: this.exgList,
      synList: this.synList,
      phoneticspelling: this.phoneticspelling
    }
  }

  send () {
    return new Promise((resolve, reject) => {
      let params = this.toObject()
      let req = GM_xmlhttpRequest({ // eslint-disable-line no-unused-vars
        method: 'POST',
        url: 'http://127.0.0.1/saveDef',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(params),
        onload: function (res) {
          if (res.status === 200 && res.responseText != null && res.responseText === '"ok"') {
            resolve()
            console.log('Definition sent')
          } else {
            reject(new Error())
            console.log('Something went wrong, transmission unsuccessful')
          }
        },
        ontimeout: function () {
          reject(new Error())
        },
        onerror: function () {
          reject(new Error())
        }
      })
    })
  }

  log () {
    console.log(this.toObject())
  }
}

function init () {
  // Static
  let word = document.getElementsByClassName('hw')[0].innerText
  let phspEl = document.getElementsByClassName('phoneticspelling')
  let phoneticspelling = phspEl[0] != null ? phspEl[0].innerText : ''
  let def = new Definition(word, phoneticspelling)

  let selectorId = 0
  const gramb = document.querySelectorAll('section.gramb')
  if (gramb[0].children[0] != null && gramb[0].children[0].classList.contains('pos')) {
    for (let el of gramb) {
      const trgList = el.getElementsByClassName('trg')
      for (let trg of trgList) {
        if (trg.children[0].tagName === 'P') {
          trg.children[0].insertAdjacentHTML('beforeEnd', `<button id='select${selectorId}' style='${BTN_STYLE}'>Select</button>`)
          let b = document.getElementById(`select${selectorId}`)
          b.oParams = {
            trg: trg,
            defText: trg.getElementsByClassName('ind')[0].innerText
          }
          b.addEventListener('click', collect)
          selectorId++

          const subSenses = trg.getElementsByClassName('subSense')
          for (let ss of subSenses) {
            let subTrg = ss.getElementsByClassName('trg')[0]
            subTrg.insertAdjacentHTML('beforeBegin', `<button id='select${selectorId}' style='${BTN_STYLE}'>Select</button>`)
            let b = document.getElementById(`select${selectorId}`)
            b.oParams = {
              trg: subTrg,
              defText: ss.getElementsByClassName('ind')[0].innerText
            }
            b.addEventListener('click', collect)
            selectorId++
          }
        }
      }
    }
    window.selectorId = selectorId
  }
  // def.log()

  function collect (event) {
    let eventSrc = event.currentTarget
    let trg = eventSrc.oParams.trg

    // exgList
    let exgList = []
    const exgElements = trg.getElementsByClassName('exg')
    for (let exg of exgElements) {
      if (exg.parentElement === trg) {
        exgList.push(exg.innerText)
      }
    }

    // Synonyms
    let synList = []
    const syn = trg.getElementsByClassName('synonyms')[0]
    if (syn != null && syn.parentElement === trg) {
      const btn = syn.querySelector('.moreInfo:not(.active) button')
      if (btn != null) {
        btn.click()
        let synElement = syn.querySelector('.exg .exs')
        synList = (synElement.innerText).split(', ')
        btn.click()
      }
    }

    // Complete Definition
    def.addInfo({
      definition: eventSrc.oParams.defText,
      exgList: exgList,
      synList: synList
    })
    def.log()

    // Confirm and send
    eventSrc.removeEventListener('click', collect)
    eventSrc.innerText = 'Send'
    eventSrc.style.background = 'lightyellow'
    eventSrc.addEventListener('click', function send (event) {
      let eventSrc = event.target
      eventSrc.innerText = 'Sending'
      eventSrc.removeEventListener('click', send)

      def.send().then(res => {
        eventSrc.style.color = 'green'
        eventSrc.innerText = 'Sent'
      }, rej => {
        eventSrc.style.color = 'red'
        eventSrc.innerText = 'Error'
      }).then(() => {
        eventSrc.disabled = true
        eventSrc.style.background = 'lightgray'
        let currId = parseInt(eventSrc.id.replace('select', ''))
        for (let i = 0; i < window.selectorId; i++) {
          if (i !== currId) {
            let btn = document.getElementById(`select${i}`)
            btn.outerHTML = ''
          }
        }
      })
    })
  }
}

setTimeout(() => {
  init()
}, 100)

const BTN_STYLE = `
margin-left: 6px;
padding: 2px 8px 3px 8px;
cursor: default;
border:2px solid #dbdee2;
color:#666666;
font:13px "Open Sans", Helvetica, Arial, sans-serif;
font-family:"Open Sans", Helvetica, Arial, sans-serif;
background:#fff;
border-radius:13px
`
